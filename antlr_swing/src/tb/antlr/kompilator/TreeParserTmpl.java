/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;

/**
 * @author tb
 *
 */
public class TreeParserTmpl extends TreeParser {

	protected GlobalSymbols gs = new GlobalSymbols();
	
	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		if (id != null) {
			System.err.println(ex.getMessage() + " in line " + id.getLine());
		} else {
			System.err.println(ex.getMessage());
		}
	}

	protected void deklaracja(String name) {
		gs.newSymbol(name);
	}
	
	protected Integer podstaw(String name, Integer v) {
		gs.setSymbol(name, v);
		return v;
	}

	protected Integer czytaj(String varA) {
		return gs.getSymbol(varA);
	}
}
