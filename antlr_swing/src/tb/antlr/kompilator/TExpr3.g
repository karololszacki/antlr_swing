tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}

prog    : (d+=decl | f+=func | e+=expr)* -> template(deklaracje={$d},funkcje={$f}, program={$e}) "
<deklaracje;separator=\" \n\">
<funkcje;separator=\" \n\">

start:
<program;separator=\" \n\"> ";

func  :
        ^(FUNCKEY i1=ID e2=expr) {deklaracja("func_" + $i1.text);} -> funkcja(nazwa={$i1.text},cialo={$e2.st})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
    
decl  :
        ^(VAR i1=ID) {deklaracja($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st}) 
        | ^(MUL   e1=expr e2=expr) -> mnoz(p1={$e1.st},p2={$e2.st}) 
        | ^(DIV   e1=expr e2=expr) -> dziel(p1={$e1.st},p2={$e2.st}) 
        | ^(PODST i1=ID   e2=expr) -> podstaw(p1={$i1.text},p2={$e2.st}) 
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ^(EXCL ID) {czytaj("func_" + $ID.text);} -> wywolaj(nazwa={$ID.text})
//        | ^(EXCL ID) -> wywolaj(nazwa={$ID.text})
        | ID   {czytaj($ID.text);} -> zmienna(p1={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,null);}
    