package tb.antlr.interpreter;

import java.util.Vector;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	GlobalSymbols gs = new GlobalSymbols();
	Vector<Integer> linie = new Vector<Integer>();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    protected void zapiszLinie(Integer wynik) {
    	linie.add(wynik);
	}

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a+b;
	}

	
	protected Integer add(String varA, Integer b) {
		Integer a = gs.getSymbol(varA);
		return a+b;
	}

	protected Integer add(Integer a, String varB) {
		Integer b = gs.getSymbol(varB);
		return a+b;
	}
	
	protected Integer add(String varA, String varB) {
		Integer a = gs.getSymbol(varA);
		Integer b = gs.getSymbol(varB);
		return a+b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a-b;
	}

	protected Integer mul(Integer a, Integer b) {
		return a*b;
	}

	protected Integer div(Integer a, Integer b) {
		return a/b;
	}

	protected void deklaracja(String name) {
		gs.newSymbol(name);
	}
	
	protected Integer podstaw(String name, Integer v) {
		gs.setSymbol(name, v);
		return v;
	}

	protected Integer czytaj(String varA) {
		return gs.getSymbol(varA);
	}
	
}
